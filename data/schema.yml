$schema: "https://json-schema.org/draft/2020-12/schema"
title: FM Tools Data Schema
description: The schema for fm-tools/data
type: object
properties:
  name:
    type: string
    description: The name of the competition tool.
  input_languages:
    type: array
    items:
      type: string
    description: The supported input languages of the tool.
  project_url:
    type: string
    format: uri
    description: The URL of the project associated with the tool.
  repository_url:
    type: string
    format: uri
    description: The URL of the tool's repository.
  spdx_license_identifier:
    type: string
    description: The SPDX license identifier of the tool.
  benchexec_toolinfo_module:
    type: string
    description: Tool-info module name of benchexec in the format benchexec.tools.<tool>.
  fmtools_format_version:
    type: string
    description: The version of the fm-tools/data format.
  fmtools_entry_maintainers:
    type: array
    items:
      type: string
    description: Maintainers of the tool.
  maintainers:
    type: array
    items:
      type: object
      properties:
        name:
          type: string
        institution:
          type: string
        country:
          type: string
        url:
          type:
            - string
            - "null"
          format: uri
      required:
        - name
        - institution
        - country
        - url
    description: Information about the maintainers of the tool.
  versions:
    type: array
    items:
      type: object
      properties:
        version:
          type: string
        doi:
          type: [string, "null"]
          pattern: '10\.5281/zenodo\.[0-9]+$'
        url:
          type:
            - string
            - "null"
          format: uri
        benchexec_toolinfo_options:
          type: array
          items:
            type: string
        required_ubuntu_packages:
          type: array
          items:
            type: string
      oneOf:
        - required:
            - version
            - benchexec_toolinfo_options
            - required_ubuntu_packages
            - doi
        - required:
            - version
            - benchexec_toolinfo_options
            - required_ubuntu_packages
            - url
    description: Information about different versions of the tool.
  competition_participations:
    type: array
    items:
      type: object
      properties:
        competition:
          type: string
          pattern: "(SV-COMP|Test-Comp) 20[1-9][0-9]$"
        track:
          type: string
          enum:
            - Validation of Correctness Witnesses 1.0
            - Validation of Correctness Witnesses 2.0
            - Validation of Violation Witnesses 1.0
            - Validation of Violation Witnesses 2.0
            - Verification
            - Test Generation
            - Validation of Test Suites
        tool_version:
          type: string
        jury_member:
          type: object
          properties:
            name:
              type: string
            institution:
              type: string
            country:
              type: string
            url:
              type:
                - string
                - "null"
          required:
            - name
            - institution
            - country
            - url
      required:
        - competition
        - track
        - tool_version
        - jury_member
    description: Information about the tool's participation in competitions.
  techniques:
    type: array
    items:
      type: string
      enum:
        - CEGAR
        - Predicate Abstraction
        - Symbolic Execution
        - Bounded Model Checking
        - k-Induction
        - Property-Directed Reachability
        - Explicit-Value Analysis
        - Numeric Interval Analysis
        - Shape Analysis
        - Separation Logic
        - Bit-Precise Analysis
        - ARG-Based Analysis
        - Lazy Abstraction
        - Interpolation
        - Automata-Based Analysis
        - Concurrency Support
        - Ranking Functions
        - Evolutionary Algorithms
        - Algorithm Selection
        - Portfolio
        - Interpolation-Based Model Checking
        - Logic Synthesis
        - Task Translation
        - Floating-Point Arithmetics
        - Guidance by Coverage Measures
        - Random Execution
        - Targeted Input Generation
    description: Techniques used by the tool.
  literature:
    type: array
    items:
      type: object
      properties:
        doi:
          type: string
          pattern: '10\.[0-9]{4,9}\/.*'
        title:
          type: string
        year:
          type: integer
      required:
        - doi
        - title
        - year
    description: Relevant literature references for the tool.
  licenses:
    type: array
    items:
      type: object
      properties:
        name: 
          type: string
        body:
          type: string
      required:
        - name
        - body
    description: Custom licenses used by the tool (applicable if no matching SPDX identifier exists).
required:
  - name
  - input_languages
  - project_url
  - spdx_license_identifier
  - benchexec_toolinfo_module
  - fmtools_format_version
  - fmtools_entry_maintainers
  - maintainers
  - versions
  - competition_participations
  - techniques
